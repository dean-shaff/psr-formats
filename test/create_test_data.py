import os

import psr_formats
import numpy as np
# import matplotlib.pyplot as plt

cur_dir = os.path.dirname(os.path.abspath(__file__))


def channelize_simulated_pulsar_data(file_path: str, nchan: int) -> str:

    dada_file = psr_formats.DADAFile(file_path).load_data()

    data = dada_file.data.copy()
    nsamples = data.shape[0]
    nsamples_downsample = nsamples // nchan
    nsamples = nsamples_downsample * nchan

    data = data[:nsamples, :, :].reshape(
        (nsamples_downsample, nchan, dada_file.npol))

    data = np.fft.fft(data, axis=0) # .astype(data.dtype) deliberately letting this be double
    split = os.path.splitext(file_path)
    channelized_file_path = split[0] + ".channelized" + split[1]
    channelized_dada_file = psr_formats.DADAFile(channelized_file_path)

    channelized_dada_file.header = dada_file.header
    channelized_dada_file.data = data
    channelized_dada_file.sampling_rate = (1.0/channelized_dada_file.bw) * nchan
    channelized_dada_file.dump_data()


def main():
    file_path = os.path.join(
        cur_dir,
        "simulated_pulsar-noise_0.8-nseries_1-npol_2-ndim_2-"
        "dm_0.1000-period_0.005757-bw_5.0-freq_1405.0.dump"
    )
    channelize_simulated_pulsar_data(file_path, 16)


if __name__ == "__main__":
    main()

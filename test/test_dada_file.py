import unittest
import os
import logging
import datetime

import numpy as np

from psr_formats.dada_file import DADAFile

current_dir = os.path.dirname(os.path.abspath(__file__))
test_data_dir = os.path.join(current_dir, "test_data")


class TestDADAFile(unittest.TestCase):

    def setUp(self):
        test_dada_file_paths = [os.path.join(
            test_data_dir,
            "simulated_pulsar-noise_0.8-nseries_1-npol_2-ndim_2-"
            "dm_0.1000-period_0.005757-bw_5.0-freq_1405.0.dump"
        ), os.path.join(
            test_data_dir,
            "simulated_pulsar-noise_0.8-nseries_1-npol_2-ndim_2-"
            "dm_0.1000-period_0.005757-bw_5.0-freq_1405.0.channelized.dump"
        ), os.path.join(
            test_data_dir,
            "simulated_pulsar-noise_0.8-nseries_1-npol_2-ndim_2-"
            "dm_0.1000-period_0.005757-bw_5.0-freq_1405.0.pre_Fold.dump"
        )]
        self.dada_files = [DADAFile(f) for f in test_dada_file_paths]

    def test_getitem(self):
        self.dada_files[0].load_data()
        self.assertTrue(self.dada_files[0]["NCHAN"] == "1")
        self.assertTrue(self.dada_files[0]["NPOL"] == "2")
        self.assertTrue(self.dada_files[0]["NDIM"] == "2")
        self.assertTrue(self.dada_files[0]["NBIT"] == "32")

        self.dada_files[1].load_data()
        self.assertTrue(self.dada_files[1]["NCHAN"] == "16")
        self.assertTrue(self.dada_files[1]["NPOL"] == "2")
        self.assertTrue(self.dada_files[1]["NDIM"] == "2")
        self.assertTrue(self.dada_files[1]["NBIT"] == "64")

        self.dada_files[2].load_data()
        self.assertTrue(self.dada_files[2]["NCHAN"] == "1")
        self.assertTrue(self.dada_files[2]["NPOL"] == "1")
        self.assertTrue(self.dada_files[2]["NDIM"] == "4")
        self.assertTrue(self.dada_files[2]["NBIT"] == "32")

    def test_setitem(self):
        self.dada_files[0]._load_data_from_file()
        self.dada_files[0]["NCHAN"] = "10"
        self.assertTrue(self.dada_files[0]["NCHAN"] == "10")

    def test_contains(self):
        self.dada_files[0]._load_data_from_file()
        self.assertTrue("NCHAN" in self.dada_files[0])

    def test_load_data_from_file(self):
        self.dada_files[0]._load_data_from_file()
        self.assertIsInstance(self.dada_files[0]._data, np.ndarray)

    def test_shape_data(self):
        self.dada_files[0]._load_data_from_file()
        data_expected = self.dada_files[0]._shape_data(self.dada_files[0].data)
        self.assertTrue(
            all([i == j for i, j in zip(data_expected.shape, (2040, 1, 2))]))

        ndim, nchan, npol = [int(self.dada_files[0][item])
                             for item in ["NDIM", "NCHAN", "NPOL"]]

        data_flat = self.dada_files[0].data

        data_shaped_ = data_flat.reshape((-1, nchan, npol*ndim))
        data_shaped = np.zeros(
            (data_shaped_.shape[0], nchan, npol),
            dtype=self.dada_files[0]["COMPLEX_DTYPE"])

        data_shaped[:, :, 0] = data_shaped_[:, :, 0] + 1j*data_shaped_[:, :, 1]
        data_shaped[:, :, 1] = data_shaped_[:, :, 2] + 1j*data_shaped_[:, :, 3]

        self.assertTrue(np.allclose(data_expected, data_shaped))

    def test_load_data(self):
        self.dada_files[0].load_data()

    def test_dump_data(self):
        self.dada_files[0].load_data()
        new_file_path = self.dada_files[0].dump_data(overwrite=False)
        self.assertFalse(new_file_path == self.dada_files[0].file_path)
        os.remove(new_file_path)

    def test_get_utc_start(self):
        self.dada_files[0].load_data()
        d = self.dada_files[0].utc_start
        expected_str = "2019-11-15 04:54:43"
        self.assertTrue(str(d) == expected_str)

    def test_set_utc_start(self):
        self.dada_files[0].load_data()
        new_utc_start = datetime.datetime.utcnow()
        self.dada_files[0].utc_start = new_utc_start

        self.dada_files[0].utc_start = new_utc_start.strftime(
            DADAFile.timestamp_formatter)

        with self.assertRaises(ValueError):
            self.dada_files[0].utc_start = "foo"

    def test_bw(self):
        fobj = self.dada_files[0].load_data()
        self.assertTrue(fobj.bw.value == 5.0)

    def test_sampling_rate(self):
        fobj = self.dada_files[0].load_data()
        self.assertTrue(fobj.sampling_rate.value == 0.2)

    def test_centre_freq(self):
        fobj = self.dada_files[0].load_data()
        self.assertTrue(fobj.centre_frequency.value == 1405)


    def test_set_data(self):
        fobj = self.dada_files[0].load_data()

        new_data = np.zeros((10, 8, 2)).astype(np.float32)

        fobj.data = new_data
        self.assertTrue(fobj.nchan == 8)
        self.assertTrue(fobj.ndat == 10)
        self.assertTrue(fobj.npol == 2)
        self.assertTrue(fobj.ndim == 1)
        self.assertTrue(fobj.nbit == 32)

        new_data = np.zeros((10, 8, 1)).astype(np.complex128)
        fobj.data = new_data
        self.assertTrue(fobj.nchan == 8)
        self.assertTrue(fobj.ndat == 10)
        self.assertTrue(fobj.npol == 1)
        self.assertTrue(fobj.ndim == 2)
        self.assertTrue(fobj.nbit == 64)


if __name__ == "__main__":
    logging.basicConfig(level=logging.DEBUG)
    unittest.main()

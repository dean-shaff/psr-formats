### v0.1.0

- Took format files from `pfb` repo.
- Updated tests to reflect new default values in `DADAFile` methods.

### v0.1.1

- Added `timestamp_formatter` class attribute
- Added `utc_start` property to `DADAFile` class. The getter returns a
`datetime.datetime` object, and the setter can take a `str` or a
`datetime.datetime` object.
- Added corresponding test for `DADAFile` class.

### v0.1.2

- Added `DataFile` to exports in __init__.py

### v0.1.3

- Added `loaded` flag to `DataFile`

### v0.2.0

- Added astropy dependency
- Added some DADAFile properties that are `astropy.units.Quantity` objects
- fixed bug where loading in data resulted in incorrect number of dimensions for non complex data.
- fixed bug where setting data wouldn't set NBIT
- Using much smaller test files that can be bundled with the source code.

### v0.2.1

- Added astropy to pyproject.toml. Not sure why it didn't show up originally.
- Added test files for test_util.py

### v0.2.2

- Updated README for pypi
